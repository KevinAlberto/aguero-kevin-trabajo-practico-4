/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2;

/**
 *
 * @author KevinAguero
 */
public class Minibus extends VehiculoPasajeros implements PrecioAlquiler{
    private int SEGURO_PLAZA = 250;

    public Minibus(int precio,String marca, String patente, boolean est, int plazas) {
        super(precio,marca, patente, est, plazas);
    }

    @Override
    public Integer calculoPrecioAlquiler(int cantidadDias) {
        return this.precioBaseAlquiler*cantidadDias+this.plazas*(SEGURO_PLAZA+PRECIO_ALQUILER_PLAZA_DIA);
    }
}
