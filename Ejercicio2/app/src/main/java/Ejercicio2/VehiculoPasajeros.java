/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2;

/**
 *
 * @author KevinAguero
 */
public abstract class VehiculoPasajeros extends Vehiculo{
    public static int PRECIO_ALQUILER_PLAZA_DIA = 50;
    public int plazas;

    public VehiculoPasajeros(int precio,String marca, String patente, boolean est, int plazas) {
        super(precio,marca, patente, est);
        this.plazas = plazas;
    }
    
    public VehiculoPasajeros(String marca, String patente, boolean est, int plazas) {
        super(marca, patente, est);
        this.plazas = plazas;
    }
    
    public void setPlazas(int plazas)
    {
        this.plazas = plazas;
    }
    
    public Integer getPlazas()
    {
        return this.plazas;
    }
}
