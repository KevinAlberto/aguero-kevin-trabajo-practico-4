/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2;

/**
 *
 * @author KevinAguero
 */
public class ComponenteExtra {
    
    private String componente;
    
    public ComponenteExtra(String componente){
        this.componente = componente;
    }
    
    public String getComponente(){
        return this.componente;
    }
}
