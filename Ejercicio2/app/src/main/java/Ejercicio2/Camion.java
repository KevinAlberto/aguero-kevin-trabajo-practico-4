/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2;

/**
 *
 * @author KevinAguero
 */
public class Camion extends VehiculoCarga{
    private int ABONO_EXTRA = 200;

    public Camion(int precio,String marca, String patente, boolean est, int kilometro) {
        super(precio,marca, patente, est, kilometro);
    }

    @Override
    public Integer calculoPrecioAlquiler(int cantidadDias) {
        if (this.kilometro<=50)
        {
            return precioBaseAlquiler*cantidadDias+300+ABONO_EXTRA;
        }
        else
        {
            return precioBaseAlquiler*cantidadDias+this.kilometro*20+ABONO_EXTRA;
        }
    }
    
}
