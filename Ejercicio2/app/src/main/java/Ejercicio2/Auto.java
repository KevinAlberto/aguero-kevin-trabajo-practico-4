/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2;


/**
 *
 * @author KevinAguero
 */
public abstract class Auto extends VehiculoPasajeros implements PrecioVenta{
    public int precioBaseVenta;
    
    public Auto(int precio,String marca, String patente, boolean est, int plazas, int preciobaseVenta) {
        super(precio,marca, patente, est, plazas);
        this.precioBaseVenta = preciobaseVenta;
    }
    
    public Auto(String marca, String patente, boolean est, int plazas, int preciobaseVenta) {
        super(marca, patente, est, plazas);
        this.precioBaseVenta = preciobaseVenta;
    }
    
    public Integer getPrecioVenta(){
        return this.precioBaseVenta;
    }
    

}
