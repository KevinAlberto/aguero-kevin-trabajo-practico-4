/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Interface;

import Ejercicio1.Curso;
import Ejercicio1.Estudiante;
import Excepciones.ExceptionCustom;
import Interface.AlumnosInscriptos;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author KevinAguero
 */
public class Principal extends javax.swing.JFrame {
    public static Curso curso = new Curso("6D");
    
    public Principal() {
        initComponents();
        alumnosInit();
        this.setLocationRelativeTo(null);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        principal = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu_Alumno = new javax.swing.JMenu();
        jMenuItem_AniadirAlumno = new javax.swing.JMenuItem();
        jMenuItem_AlumnosIncriptos = new javax.swing.JMenuItem();
        jMenuItem_AlumnosAprobados = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem_Salir = new javax.swing.JMenuItem();
        jMenu_Ciudad = new javax.swing.JMenu();
        jMenuItem_CiudadExCapital = new javax.swing.JMenuItem();
        jMenu_Curso = new javax.swing.JMenu();
        jMenuItem_PorcentajeAprobados = new javax.swing.JMenuItem();
        jMenuItem_Promedio = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        principal.setBackground(new java.awt.Color(0, 0, 102));

        javax.swing.GroupLayout principalLayout = new javax.swing.GroupLayout(principal);
        principal.setLayout(principalLayout);
        principalLayout.setHorizontalGroup(
            principalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 650, Short.MAX_VALUE)
        );
        principalLayout.setVerticalGroup(
            principalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 377, Short.MAX_VALUE)
        );

        jMenu_Alumno.setText("Alumno");

        jMenuItem_AniadirAlumno.setText("A�adir Alumno");
        jMenuItem_AniadirAlumno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_AniadirAlumnoActionPerformed(evt);
            }
        });
        jMenu_Alumno.add(jMenuItem_AniadirAlumno);

        jMenuItem_AlumnosIncriptos.setText("Alumnos Inscriptos");
        jMenuItem_AlumnosIncriptos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_AlumnosIncriptosActionPerformed(evt);
            }
        });
        jMenu_Alumno.add(jMenuItem_AlumnosIncriptos);

        jMenuItem_AlumnosAprobados.setText("Alumnos Aprobados");
        jMenuItem_AlumnosAprobados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_AlumnosAprobadosActionPerformed(evt);
            }
        });
        jMenu_Alumno.add(jMenuItem_AlumnosAprobados);
        jMenu_Alumno.add(jSeparator1);

        jMenuItem_Salir.setText("Salir");
        jMenuItem_Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_SalirActionPerformed(evt);
            }
        });
        jMenu_Alumno.add(jMenuItem_Salir);

        jMenuBar1.add(jMenu_Alumno);

        jMenu_Ciudad.setText("Ciudad Alumno");

        jMenuItem_CiudadExCapital.setText("Ciudades excepto capital");
        jMenuItem_CiudadExCapital.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_CiudadExCapitalActionPerformed(evt);
            }
        });
        jMenu_Ciudad.add(jMenuItem_CiudadExCapital);

        jMenuBar1.add(jMenu_Ciudad);

        jMenu_Curso.setText("Curso");

        jMenuItem_PorcentajeAprobados.setText("Porcentaje de aprobados");
        jMenuItem_PorcentajeAprobados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_PorcentajeAprobadosActionPerformed(evt);
            }
        });
        jMenu_Curso.add(jMenuItem_PorcentajeAprobados);

        jMenuItem_Promedio.setText("Promedio Alumnos");
        jMenuItem_Promedio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_PromedioActionPerformed(evt);
            }
        });
        jMenu_Curso.add(jMenuItem_Promedio);

        jMenuBar1.add(jMenu_Curso);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(principal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(principal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void alumnosInit(){
        Estudiante estudiante1 = new Estudiante("Kevin Aguero", "Valle Viejo");
        Estudiante estudiante2 = new Estudiante("Rodrigo Bustamantes", "San Antonio");
        Estudiante estudiante3 = new Estudiante("Patricia Perez", "Capital");
        
        try{
            estudiante1.exceptionAlumno(19, 43613164);
            estudiante2.exceptionAlumno(22, 44964383);
            estudiante3.exceptionAlumno(21, 40964383);
        }catch(ExceptionCustom e){
            System.out.println(e.getMessage());
        }
        
        try{
            estudiante1.agregarCalificacion(7);
            estudiante1.agregarCalificacion(9);   
            estudiante1.agregarCalificacion(8);
            
            estudiante2.agregarCalificacion(4);
            estudiante2.agregarCalificacion(2);
            estudiante2.agregarCalificacion(3);
            
            estudiante3.agregarCalificacion(10);
            estudiante3.agregarCalificacion(6);
            estudiante3.agregarCalificacion(8);
            
        }catch(ExceptionCustom e){
            System.out.println(e.getMessage());
        }
        
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);
    }
    
    public Curso getCurso(){
        return curso;
    }
    
    private void jMenuItem_SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_SalirActionPerformed
        
        Salir salir = new Salir();
        salir.setVisible(true);
        salir.setLocation(principal.getWidth()/2 - salir.getWidth()/2, principal.getHeight()/2 - salir.getHeight()/2);
        principal.add(salir);
    }//GEN-LAST:event_jMenuItem_SalirActionPerformed

    private void jMenuItem_AniadirAlumnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_AniadirAlumnoActionPerformed
        
        AltaAlumno alta = new AltaAlumno(curso);
        alta.setVisible(true);
        alta.setLocation(principal.getWidth()/2 - alta.getWidth()/2, principal.getHeight()/2 - alta.getHeight()/2);
        principal.add(alta);
    }//GEN-LAST:event_jMenuItem_AniadirAlumnoActionPerformed

    private void jMenuItem_AlumnosIncriptosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_AlumnosIncriptosActionPerformed

        if (curso.existeEstudiante()){
            AlumnosInscriptos inscriptos = new AlumnosInscriptos(curso);
            inscriptos.setVisible(true);
            inscriptos.setLocation(principal.getWidth()/2 - inscriptos.getWidth()/2, principal.getHeight()/2 - inscriptos.getHeight()/2);
            principal.add(inscriptos);
        }
        else{
            JOptionPane.showMessageDialog(null,"�No existe ningun alumno en el curso! Por favor a�ada al menos uno.");
        }
    }//GEN-LAST:event_jMenuItem_AlumnosIncriptosActionPerformed

    private void jMenuItem_AlumnosAprobadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_AlumnosAprobadosActionPerformed
        if (curso.existeEstudiante()){
            AlumnosAprobados aprobados = new AlumnosAprobados(curso);
            aprobados.setVisible(true);
            aprobados.setLocation(principal.getWidth()/2 - aprobados.getWidth()/2, principal.getHeight()/2 - aprobados.getHeight()/2);
            principal.add(aprobados);
        }
        else{
            JOptionPane.showMessageDialog(null,"�No existe ningun alumno en el curso! Por favor a�ada al menos uno.");
        }
    }//GEN-LAST:event_jMenuItem_AlumnosAprobadosActionPerformed

    private void jMenuItem_CiudadExCapitalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_CiudadExCapitalActionPerformed
        if (curso.existeEstudiante()){
            CiudadExceptoCapital ciudades = new CiudadExceptoCapital(curso.ciudadesExceptoCapital());
            ciudades.setVisible(true);
            ciudades.setLocation(principal.getWidth()/2 - ciudades.getWidth()/2, principal.getHeight()/2 - ciudades.getHeight()/2);
            principal.add(ciudades);
        }
        else{
            JOptionPane.showMessageDialog(null,"No existe ningun alumno en el curso o que sea del interior");
        }
    }//GEN-LAST:event_jMenuItem_CiudadExCapitalActionPerformed

    private void jMenuItem_PorcentajeAprobadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_PorcentajeAprobadosActionPerformed
        if (curso.existeEstudiante()){
            PorcentajeAprobados porcentaje = new PorcentajeAprobados(curso.porcentajeDeAprobados());
            porcentaje.setVisible(true);
            porcentaje.setLocation(principal.getWidth()/2 - porcentaje.getWidth()/2, principal.getHeight()/2 - porcentaje.getHeight()/2);
            principal.add(porcentaje);
        }
        else{
            JOptionPane.showMessageDialog(null,"�No existe ningun alumno en el curso! Por favor a�ada al menos uno.");
        }
    }//GEN-LAST:event_jMenuItem_PorcentajeAprobadosActionPerformed

    private void jMenuItem_PromedioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_PromedioActionPerformed
        if (curso.existeEstudiante()){
            PromedioAlumnos promedio = new PromedioAlumnos(curso.promedioDeCalificaciones(), curso.unDesastre());
            promedio.setVisible(true);
            promedio.setLocation(principal.getWidth()/2 - promedio.getWidth()/2, principal.getHeight()/2 - promedio.getHeight()/2);
            principal.add(promedio);
        }
        else{
            JOptionPane.showMessageDialog(null,"�No hay calificaciones registradas!");
        }
    }//GEN-LAST:event_jMenuItem_PromedioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem_AlumnosAprobados;
    private javax.swing.JMenuItem jMenuItem_AlumnosIncriptos;
    private javax.swing.JMenuItem jMenuItem_AniadirAlumno;
    private javax.swing.JMenuItem jMenuItem_CiudadExCapital;
    private javax.swing.JMenuItem jMenuItem_PorcentajeAprobados;
    private javax.swing.JMenuItem jMenuItem_Promedio;
    private javax.swing.JMenuItem jMenuItem_Salir;
    private javax.swing.JMenu jMenu_Alumno;
    private javax.swing.JMenu jMenu_Ciudad;
    private javax.swing.JMenu jMenu_Curso;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPanel principal;
    // End of variables declaration//GEN-END:variables


}
