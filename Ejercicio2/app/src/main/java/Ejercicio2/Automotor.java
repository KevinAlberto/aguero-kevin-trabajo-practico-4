package Ejercicio2;

import Excepciones.ExceptionPatente;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class Automotor 
{
    private static Automotor automotor;
    public static List <Vehiculo> lista_vehiculos = new ArrayList<Vehiculo>();
    
    private Automotor() {
    }
    
    public static Automotor instancia(){
        
        if (automotor == null){
            return automotor = new Automotor();
        }
        else{
            return automotor;
        }
    }

    public void registroUnVehiculo(Vehiculo vehiculo)
    {
        lista_vehiculos.add(vehiculo);
    }

    public void eliminarVehiculo(int index){
        lista_vehiculos.remove(index);
    }
    
    public boolean existeAutoParaVender(){
        for (Vehiculo i : lista_vehiculos){
            if(i instanceof Auto && (i.getEstado() == true)){
                return true;
            }
        }
        return false;
    }
    
    public boolean existeVehiculoParaAlquilar(){
        for (Vehiculo i : lista_vehiculos){
            if((i.getEstado() == true)){
                return true;
            }
        }
        return false;
    }
    
    public List <Auto> ordenarAutos(){
        
        List <Auto> autos = new ArrayList<Auto>();
        for(Vehiculo i : lista_vehiculos){
            if(i instanceof Auto){
                autos.add((Auto) i);
            }
        }
        Set<Auto> sinRepetir = new HashSet<>(autos);
        autos.clear();
        autos.addAll(sinRepetir);
        
        Collections.sort(autos, new Comparator<Auto>() {
            @Override
            public int compare(Auto arg0, Auto arg1) {
                return Integer.compare(arg0.precioBaseVenta,arg1.precioBaseVenta);
            }
        });
        
        return autos;
    }
    
    public void compararPatente(String patente)
    throws ExceptionPatente{
        for (Vehiculo i : lista_vehiculos){
            if (i.getPatente().equals(patente)){
                throw new ExceptionPatente("Error, se registr� dos vehiculos con la misma patente.");
            }
        }
    }

    public Vehiculo buscarVehiculo(String patente)
    {
        Vehiculo v1 = null;
        for (Vehiculo i : lista_vehiculos)
        {
            if (i.getPatente().equals(patente))
            {
                v1 = i;
            }
        }
        return v1;
    }
}
