/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2;

/**
 *
 * @author KevinAguero
 */
public abstract class VehiculoCarga extends Vehiculo implements PrecioAlquiler{
    private static int PRECIO_ALQUILER_MENOS_50KM = 300;
    public int kilometro;
    private int PRECIO_ALQUILER_MAS_50KM = 20*kilometro;
    
    public VehiculoCarga(int precio,String marca, String patente, boolean est, int kilometro) {
        super(precio, marca, patente, est);
        this.kilometro = kilometro;
    }
    
    public void setKilometrosRecorridos(int kilometrosRecorridos)
    {
        this.kilometro = kilometrosRecorridos;
    }

    public Integer getKilometrosRecorridos()
    {
        return kilometro;
    }
}
