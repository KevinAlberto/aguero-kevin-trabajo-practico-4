/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio1;

import java.util.Comparator;

/**
 *
 * @author KevinAguero
 */
public class CompararEstudiantePorDNI implements Comparator{

    @Override
    public int compare(Object o1, Object o2) {
        Estudiante e1 = (Estudiante) o1;
        Estudiante e2 = (Estudiante) o2;
        int respuesta = -1;
        if (e1.getDNI() == e2.getDNI()){
            respuesta = e1.getEdad() - e2.getEdad();
        }
        else{
            respuesta = e1.getDNI() - e2.getDNI();
        }
        return respuesta;
    }
    
}
