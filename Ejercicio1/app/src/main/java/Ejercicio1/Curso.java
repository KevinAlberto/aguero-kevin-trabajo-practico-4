package Ejercicio1;
import Excepciones.ExceptionCustom;
import Excepciones.ExceptionInterfaz;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
public class Curso 
{
    private String nombreCurso;
    private static int numero;
    
    public List <Estudiante> estudiantes;
    
    public Curso(String nombreCurso)
    {
        this.nombreCurso = nombreCurso;
        estudiantes = new ArrayList<Estudiante>();
    }

    public String getNombreCurso()
    {
        return this.nombreCurso;
    }
    
    public void existeEstudianteDNI(int dni) throws ExceptionInterfaz
    {
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.getDNI() == dni)
            {
                throw new ExceptionInterfaz("DNI Es el mismo");
                
            }
        }
    }
    
    public List <Estudiante> estudiantes()
    {
        Collections.sort(estudiantes, new CompararEstudiantePorDNI());
        
        return this.estudiantes;
    }
    public void agregarEstudiante(Estudiante estudiante)
    {
        this.estudiantes.add(estudiante);
    }
    public int cantidadDeEstudiantesInscriptos()
            throws ExceptionCustom{
        if (this.estudiantes.size() == 0){
            throw new ExceptionCustom("\nError, no hay estudiantes en el curso.");
        }
        return this.estudiantes.size();
    }

    public int promedioAlumno(Estudiante alumno)
    {
        int promedio = 0;
        for (int i : alumno.getCalificacion())
        {
            promedio += i;
        }
        return promedio/alumno.getCalificacion().size();
    }

    public List <Estudiante> estudiantesAprobados()
    {
        List <Estudiante> estudiantesAprobados = new ArrayList<Estudiante>();
        for (Estudiante i : this.estudiantes)
        {
            if (promedioAlumno(i) >= 4)
            {
                estudiantesAprobados.add(i);
            }   
        }
        Collections.sort(estudiantesAprobados, new CompararEstudiantePorNota());
        return estudiantesAprobados;
    }

    public void resetearNotas(){
        for (Estudiante estudiante : estudiantes)
        {
            estudiante.getCalificacion().clear();
            estudiante.getCalificacion().add(0);
        }
    }

    public boolean existeEstudiante()
    {
        return !estudiantes.isEmpty();
    }

    public boolean existeEstudianteLlamado(String nombre)
    {
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.getNombre() == nombre)
            {
                
                return true;           
            }
        }
        return false;
    }

    public boolean existeEstudianteConNotaDiez()
    {
        for (Estudiante estudiante : this.estudiantes)
        {
            if (promedioAlumno(estudiante) == 10)
            {
                return true;
            }
        }
        return false;
    }

    public int porcentajeDeAprobados()
    {
        int aprobados = 0;
        for (Estudiante estudiante : this.estudiantes)
        {
            if (promedioAlumno(estudiante) >= 4)
            {
                aprobados += 1;
            }
        }
        return (aprobados*100)/estudiantes.size();
    }

    public int promedioDeCalificaciones()
    {
        int cantidadPromedios=0;
        for (Estudiante estudiante : this.estudiantes)
        {
            cantidadPromedios += promedioAlumno(estudiante);
        }
        return cantidadPromedios/estudiantes.size();
    }

    public static void metodo(){
        System.out.println("Es estatico");
    }
    
    public List <String> ciudadesExceptoCapital()
    {
        List <String> ciudades = new ArrayList<String>();
        
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.getLugarNacimiento() != "Capital")
            {
                ciudades.add(estudiante.getLugarNacimiento());
            }
        }
        Set<String> sinRepetir = new HashSet<String>(ciudades);
        ciudades.clear();
        ciudades.addAll(sinRepetir);

        
        Collections.sort(ciudades, new Comparator<String>() {
            @Override
            public int compare(String arg0, String arg1) {
                return arg0.compareTo(arg1);    
            }
        });

        return ciudades;
    }

    public List <Estudiante> estudiantesDelInteriorProvincial()
    {
        List <Estudiante> estudiantesInterior = new ArrayList<Estudiante>();
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.getLugarNacimiento() != "Capital")
            {
                estudiantesInterior.add(estudiante);
            }
        }
        Collections.sort(estudiantesInterior, new Comparator<Estudiante>() {
            @Override
            public int compare(Estudiante arg0, Estudiante arg1) {
                int valor;
                valor = arg0.compareTo(arg1.getNombre());
                if (valor == 0){
                    valor = arg0.getLugarNacimiento().compareTo(arg1.getLugarNacimiento());
                }
                return valor;
            }
        });
        
        return estudiantesInterior;

    }

    public boolean unDesastre()
    {
        for (Estudiante estudiante : this.estudiantes)
        {
            if (promedioAlumno(estudiante) >= 4)
            {
                return false;
            }
        }
        return true;
    }
}
