package Ejercicio2;

public abstract class Vehiculo
{
    private String marca;
    private String patente;
    private boolean estado;
    public int precioBaseAlquiler;

    public Vehiculo(int precio,String marca, String patente, boolean est)
    {
        this.marca = marca;
        this.patente = patente;
        this.estado = est;
        this.precioBaseAlquiler = precio;
    }
    
    public Vehiculo(String marca, String patente, boolean est){
        this.marca = marca;
        this.patente = patente;
        this.estado = est;
    }
    
    public String getMarca()
    {
        return marca;
    }

    public void setMarca(String marca)
    {
        this.marca = marca;
    }
    
    public String getPatente()
    {
        return patente;
    }
    
    public boolean getEstado()
    {
        return estado;
    }
    
    public Integer getPrecioAlquiler()
    {
        return precioBaseAlquiler;
    }
    
    public void setPrecioAlquiler(int precio)
    {
        precioBaseAlquiler = precio;
    }

    public void setEstado (boolean estado)
    {
        this.estado = estado;
    }
    
    public void setPatente(String patente)
    {
        this.patente = patente;
    }
}
