/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KevinAguero
 */
public class Auto0Km extends Auto{

    List <ComponenteExtra> lista_componentes = new ArrayList<ComponenteExtra>();
    
    public Auto0Km(String marca, String patente, boolean est, int plazas, int preciobaseVenta, List <ComponenteExtra> componentes) {
        super(marca, patente, est, plazas, preciobaseVenta);
        this.lista_componentes = componentes;
    }

    @Override
    public void calcularPrecioVenta() {

        for(ComponenteExtra comp : lista_componentes){
            if (comp.getComponente().equals("Aire acondicionado")){
                this.precioBaseVenta = (int) (this.precioBaseVenta*1.02);
            }
            else if(comp.getComponente().equals("Levanta cristales electricos")){
                this.precioBaseVenta = (int) (this.precioBaseVenta*1.05);
            }
            else if(comp.getComponente().equals("Alarma")){
                this.precioBaseVenta = (int) (this.precioBaseVenta*1.01);
            }
        }
    }
}
