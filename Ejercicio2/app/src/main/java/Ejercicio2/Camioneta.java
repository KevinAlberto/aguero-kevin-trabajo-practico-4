/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2;

/**
 *
 * @author KevinAguero
 */
public class Camioneta extends VehiculoCarga{

    public Camioneta(int precio,String marca, String patente, boolean est, int kilometro) {
        super(precio,marca, patente, est, kilometro);
    }

    @Override
    public Integer calculoPrecioAlquiler(int cantidadDias) {
        if (this.kilometro<=50)
        {
            return precioBaseAlquiler*cantidadDias+300;
        }
        else
        {
            return precioBaseAlquiler*cantidadDias+this.kilometro*20;
        }
    }
}
