/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2;

/**
 *
 * @author KevinAguero
 */
public class AutoUsado extends Auto implements PrecioAlquiler{
    
    public AutoUsado(int precio,String marca, String patente, boolean est, int plazas, int preciobaseVenta) {
        super(precio,marca, patente, est, plazas, preciobaseVenta);
    }

    @Override
    public void calcularPrecioVenta() {
        this.precioBaseVenta = (int) (this.precioBaseVenta*1.35); 
    }

    @Override
    public Integer calculoPrecioAlquiler(int cantidadDias) {
        return this.precioBaseAlquiler*cantidadDias+this.plazas*PRECIO_ALQUILER_PLAZA_DIA;
    }
    
}
