/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Excepciones;

/**
 *
 * @author KevinAguero
 */
public class ExceptionPatente extends Exception{
    
    public ExceptionPatente(String mensaje){
        super(mensaje);
    }
    
}
