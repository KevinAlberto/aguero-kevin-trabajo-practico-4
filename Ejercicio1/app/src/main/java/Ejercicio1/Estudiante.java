package Ejercicio1;
import java.util.ArrayList;
import Excepciones.ExceptionCustom;

public class Estudiante implements Comparable{
    private String nombre;
    private String lugarNacimiento;
    private int dni;
    private int edad;

    private ArrayList<Integer> calificacion;

    public Estudiante(String nombre, String lugarNacimiento){
        this.nombre = nombre;
        this.lugarNacimiento = lugarNacimiento;
        calificacion = new ArrayList<Integer>();
    }

    public void exceptionAlumno(int edad, int dni)
            throws ExceptionCustom{
        
        if (edad <= 0){
            
            throw new ExceptionCustom("Dato de edad invalido.");
        }
        this.edad = edad;
        if (dni <= 0){
            throw new ExceptionCustom("Dato de DNI invalido.");
        } 
        this.dni = dni;
        
    }
    
    public void agregarCalificacion(int calificacion)
            throws ExceptionCustom{
        if (calificacion <=0 || calificacion>10){
            throw new ExceptionCustom("\n---------------Error excepcion, dato de nota de examen invalido.---------------\n");
        }
        this.calificacion.add(calificacion);
    }

    public String getLugarNacimiento()
    {
        return this.lugarNacimiento;
    }
    public String getNombre()
    {
        return this.nombre;
    }
    public Integer getEdad()
    {
        return this.edad;
    }
    public Integer getDNI()
    {
        return this.dni;
    }
    public ArrayList<Integer> getCalificacion()
    {
        return this.calificacion;
    }

    @Override
    public int compareTo(Object obj) {
        Estudiante e = (Estudiante) obj;
        return this.nombre.compareTo(e.getNombre());
    }
}
