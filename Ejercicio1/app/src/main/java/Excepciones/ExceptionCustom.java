/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Excepciones;

/**
 *
 * @author KevinAguero
 */
public class ExceptionCustom extends Exception{
    
    public ExceptionCustom(String mensaje){
        super(mensaje);
    }
    
}
